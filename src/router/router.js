import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Navbar from "../components/navbar/index"

export default function Routing() {
    return (
        <Router>
            <Switch>
                <Route exact path="/">
                    <Navbar />
                </Route>
            </Switch>
        </Router>
    )
}
