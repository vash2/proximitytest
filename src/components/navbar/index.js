import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import './style.css';
import Video from '../../assets/video.mp4';
import logo from '../../assets/logo.png'

export default function Navbar() {
    const [isActive, setActive] = useState(false);

    const toggleClass = () => {
        setActive(!isActive);
    }


    return (
        <nav className="navbar">
            <video className="video" autostart autoPlay loop src={Video} type="video/mp4" />
            <img className="navbar-logo" src={logo} type="image/png" />
            
                

            <div class="push"></div>
            <div className="navbar-right">
                <ul className={isActive ? "active" : null}>
                    <Link to="/">About</Link>
                    <Link to="/">Work</Link>
                    <Link to="/">Latest</Link>
                    <Link to="/">People & Careers</Link>
                    <Link to="/">Contact</Link>
                </ul>
            </div>
            <div className="navbar-language">
            <button className="btn-language">EN</button>
            <button className="btn-language">EN</button>
            </div>
            <div className="menu-icon" onClick={toggleClass}>
                    <i className={isActive? 'fas fa-times' : 'fas fa-bars'}></i>

                </div>

        </nav>
    )
}